%global npm_name @bitwarden/cli

%{?nodejs_default_filter}

Name:           bitwarden-cli
Version:        1.13.3
Release:        1%{?dist}
Summary:        Bitwarden vault CLI tool

# json-schema is (AFLv2.1 or BSD)
License:        GPLv3 and 0BSD and (AFL or BSD) and ASL 2.0 and BSD and (BSD or GPLv3) and ISC and MIT and Unlicense
URL:            https://github.com/bitwarden/cli
Source0:        https://registry.npmjs.org/%{npm_name}/-/cli-%{version}.tgz#/%{name}-%{version}.tgz
Source1:        @%{name}-%{version}-nm-prod.tgz
Source3:        @%{name}-%{version}-bundled-licenses.txt

BuildArch:      noarch
ExclusiveArch:  %{nodejs_arches} noarch

Requires:       nodejs
BuildRequires:  nodejs-devel

%description
The Bitwarden CLI is a powerful, full-featured command-line interface (CLI) tool
to access and manage a Bitwarden vault. The CLI is written with TypeScript and
Node.js


%prep
%autosetup -p1 -n package
cp %{SOURCE3} .


%build
# Setup bundled node modules
tar xfz %{SOURCE1}
# remove unnecessary files
# #!/usr/bin/env node, sh, bash
rm -rf node_modules_prod/ajv/scripts/
# bash completion script
rm -rf node_modules_prod/dashdash/etc
# empty log
rm -f node_modules_prod/nwsapi/dist/lint.log
# #!/usr/bin/env ./node_modules/.bin/coffee
rm -rf node_modules_prod/performance-now/test/
# scripts
rm -rf node_modules_prod/pn/scripts/

# fix shebangs for node
for f in $(grep -l '/usr/bin/env node' build/bw.js node_modules_prod/is-docker/cli.js node_modules_prod/*/bin/*);
do
  sed -i 's|#!/usr/bin/env node$|#!/usr/bin/node|' $f
done
# fix executable permissions
chmod 755 build/bw.js node_modules_prod/tldjs/bin/update.js

# js.map and json files should not be executable
find node_modules_prod -type f -executable \( -name '*.js.map' -o -name '*.json' \) -exec chmod 644 '{}' +

# compress bundled manpages
for m in sshpk;
do
  gzip node_modules_prod/$m/man/man*/*.?
done


# remove hidden files and directories
find node_modules_prod -type f -name '.*' -delete
find node_modules_prod -type d -name .bin -prune -o -type d -name '.*' -exec rm -rf '{}' +

mkdir -p node_modules
pushd node_modules
ln -s ../node_modules_prod/* .
ln -s ../node_modules_prod/.bin .
popd


%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{npm_name}
# Copy over bundled nodejs modules
cp -pr node_modules node_modules_prod %{buildroot}%{nodejs_sitelib}/%{npm_name}

cp -pr build package.json %{buildroot}%{nodejs_sitelib}/%{npm_name}

mkdir -p %{buildroot}%{_bindir}
pushd %{buildroot}%{_bindir}
ln -s ../../%{nodejs_sitelib}/%{npm_name}/build/bw.js bw


%files
%license LICENSE.txt @%{name}-%{version}-bundled-licenses.txt
%doc README.md
%attr(0755,root,root) %{_bindir}/bw
# dir #{nodejs_sitelib}/@bitwarden
%{nodejs_sitelib}/%{npm_name}


%changelog
* Tue Jan 19 2021 Michel Alexandre Salim <salimma@fedoraproject.org> - 1.13.3-1
- Initial package
