#!/bin/sh
DIR=`dirname $1`
PKGNAME=`basename $1 | sed -e 's|.spec$||'`

git rm $1 && git commit -sS -m "- $PKGNAME" && ./push.sh

rm -i $DIR/$PKGNAME-[0-9]*.src.rpm
fedora "rm -i public_html/specs/$DIR/$PKGNAME-[0-9]*.src.rpm"

