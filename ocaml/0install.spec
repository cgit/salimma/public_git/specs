%global cache_dir /var/cache/0install.net

Name:           0install
Version:        2.6.1
Release:        2%{?dist}
Summary:        A decentralized cross-distribution software installation system

License:        LGPLv2+
URL:            http://0install.net
Source0:        http://downloads.sf.net/project/zero-install/%{name}/%{version}/%{name}-%{version}.tar.bz2
# Temporarily disable tests, some failures within mock env
Patch0:         %{name}-2.6-no-tests.patch
# multilib: use lib64 instead of lib on 64-bit arches
Patch1:         %{name}-2.6.1-lib64.patch

ExclusiveArch:  %{ocaml_arches}

BuildRequires:  ocaml >= 3.10.0
BuildRequires:  ocaml-biniou-devel
BuildRequires:  ocaml-easy-format-devel
BuildRequires:  ocaml-findlib
BuildRequires:  ocaml-camlp4-devel
BuildRequires:  ocaml-curl-devel
BuildRequires:  ocaml-extlib-devel
BuildRequires:  ocaml-lablgtk-devel
BuildRequires:  ocaml-lwt-devel
# not packaged yet, optional
# BuildRequires:  ocaml-obus-devel
BuildRequires:  ocaml-ounit-devel
BuildRequires:  ocaml-ssl-devel
BuildRequires:  ocaml-xmlm-devel
BuildRequires:  ocaml-yojson-devel
BuildRequires:  glib2-devel
BuildRequires:  libev-devel
BuildRequires:  gettext
BuildRequires:  desktop-file-utils

Requires(pre):  shadow-utils

# for icons
Requires:       hicolor-icon-theme

Provides:       zeroinstall-injector = %{version}-%{release}
Obsoletes:      zeroinstall-injector < 2.4-1%{?dist}

%description
Zero Install is a decentralized cross-distribution software
installation system available under the LGPL. It allows software
developers to publish programs directly from their own web-sites,
while supporting features familiar from centralized distribution
repositories such as shared libraries, automatic updates and digital
signatures. It is intended to complement, rather than replace, the
operating system's package management. 0install packages never
interfere with those provided by the distribution.

0install does not define a new packaging format; unmodified tarballs
or zip archives can be used. Instead, it defines an XML metadata
format to describe these packages and the dependencies between them. A
single metadata file can be used on multiple platforms (e.g. Ubuntu,
Debian, Fedora, openSUSE, Mac OS X and Windows), assuming binary or
source archives are available that work on those systems.

0install also has some interesting features not often found in
traditional package managers. For example, while it will share
libraries whenever possible, it can always install multiple versions
of a package in parallel when there are conflicting
requirements. Installation is always side-effect-free (each package is
unpacked to its own directory and will not touch shared directories
such as /usr/bin), making it ideal for use with sandboxing
technologies and virtualization.

The XML file describing the program's requirements can also be
included in a source-code repository, allowing full dependency
handling for unreleased developer versions. For example, a user can
clone a Git repository and build and test the program, automatically
downloading newer versions of libraries where necessary, without
interfering with the versions of those libraries installed by their
distribution, which continue to be used for other software.


%prep
%setup -q
%patch0 -p1 -b .no-tests
%if "%{_lib}" == "lib64"
%patch1 -p1 -b .lib64
%endif


%build
make


%install
make install_system DESTDIR=$RPM_BUILD_ROOT

# fix permission of shared objects
chmod +x $RPM_BUILD_ROOT%{_libdir}/0install.net/gui_gtk.cmxs

mkdir -p "$RPM_BUILD_ROOT%{cache_dir}/implementations"

desktop-file-validate \
    $RPM_BUILD_ROOT%{_datadir}/applications/0install.desktop

%find_lang zero-install


%pre
# Add the "zeroinst" user.
# This is not used by default, but is required if you want to
# set up sharing of downloads later.
getent group  zeroinst >/dev/null || groupadd -r zeroinst
getent passwd zeroinst >/dev/null || \
useradd -r -g zeroinst -d %{cache_dir} -s /sbin/nologin \
    -c "Zero Install shared cache" zeroinst
exit 0

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :

%postun
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :


%files -f zero-install.lang
%doc COPYING README.md
%{_bindir}/0*
%{_libdir}/0install.net
%{_datadir}/0install.net
%{_datadir}/applications/0install.desktop
%{_datadir}/icons/hicolor/*/apps/zeroinstall.*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/0*
%dir %{_datadir}/fish
%dir %{_datadir}/fish/completions
%{_datadir}/fish/completions/0*.fish
%dir %{_datadir}/zsh
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_0*
%{_mandir}/man1/0*
%dir %{cache_dir}
%attr(755,zeroinst,zeroinst) %{cache_dir}/implementations


%changelog
* Sat Feb 15 2014 Michel Salim <salimma@fedoraproject.org> - 2.6.1-2
- Spec clean-up
- Fix permission of shared objects so they get properly handled when
  generating debuginfo

* Fri Feb 14 2014 Michel Salim <salimma@fedoraproject.org> - 2.6.1-1
- Update to 2.6.1
- Install to proper libdir depending on the actual architecture
- Fix ExclusiveArch, build requirements and obsoletion
- Fix license tag
- Yank old changelog from zero-install

* Mon Jan 20 2014 Michel Salim <salimma@fedoraproject.org> - 2.6-1
- Update to 2.6

* Mon Jan 20 2014 Michel Salim <salimma@fedoraproject.org> - 2.5.1-1
- Rename package to 0install
- Update to 2.5.1
