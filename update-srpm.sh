#!/bin/sh
DIR=`dirname $1`
PKGNAME=`basename $1 | sed -e 's|.spec$||'`

for srpm in $DIR/$PKGNAME-[0-9]*.src.rpm; do
  rm -i $srpm
  fedora "rm -i public_html/specs/$srpm"
done

rpmbuild -bs $1 && mv ~/rpmbuild/SRPMS/$PKGNAME*.src.rpm $DIR/ \
    && ./upload.sh $DIR/$PKGNAME-*.src.rpm
