%global srcname readline

# Tests curretly fail
# Testing readline.lua 2.7, 15apr2020 on Linux
# ok 1 - type of RL is table
# About to test the Alternative Interface ...
# Tab-completion should work: readline: readline_callback_read_char() called with no handler!
%bcond_with tests

Name:           lua-%{srcname}
Version:        2.7
Release:        1%{?dist}
Summary:        Lua interface to the readline and history libraries

License:        MIT
URL:            https://www.pjb.com.au/comp/lua/%{srcname}.html
Source0:        https://www.pjb.com.au/comp/lua/%{srcname}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  lua-devel >= 5.1
%if %{with tests}
BuildRequires:  lua-posix
%endif
BuildRequires:  readline-devel
Requires:       lua-posix

%description
This Lua module offers a simple calling interface to the GNU Readline/History
Library.


%prep
%autosetup -n %{srcname}-%{version} -p1


%build
gcc %{optflags} -fPIC \
  $(pkgconf --cflags --libs lua) $(pkgconf --cflags --libs readline) \
  -c C-%{srcname}.c
gcc -shared -o C-%{srcname}.so C-%{srcname}.o

%install
mkdir -p %{buildroot}%{lua_libdir}
mkdir -p %{buildroot}%{lua_pkgdir}
cp -p C-%{srcname}.so %{buildroot}%{lua_libdir}/
cp -p %{srcname}.lua %{buildroot}%{lua_pkgdir}/


%if %{with tests}
%check
lua test/test_rl.lua
%endif


%files
%doc doc/%{srcname}.html
%{lua_libdir}/C-%{srcname}.so
%{lua_pkgdir}/%{srcname}.lua


%changelog
* Wed Nov 25 2020 Michel Alexandre Salim <salimma@fedoraproject.org> - 2.7-1
- Initial package
