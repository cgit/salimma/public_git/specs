#!/bin/sh
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail

MOCK_ARGS="--chain --localrepo=./repo"
CRATE=$1
shift

if [ -f $(pwd)/pkg.conf ]; then
  source $(pwd)/pkg.conf
fi

rust2rpm -s ${CRATE} $@
mv ${CRATE}*.crate ~/rpmbuild/SOURCES/
PATCH=${CRATE}-fix-metadata.diff
[ -f ${PATCH} ] && cp -p ${PATCH} ~/rpmbuild/SOURCES/
rpmbuild -bs rust-${CRATE}.spec
mock ${MOCK_ARGS} ~/rpmbuild/SRPMS/rust-${CRATE}*.src.rpm
