# Generated by rust2rpm 20
%bcond_without check
%global debug_package %{nil}

%global crate mmtk

Name:           rust-%{crate}
Version:        0.9.0
Release:        %autorelease
Summary:        Framework for the design and implementation of high-performance and portable memory managers

# Upstream license specification: MIT OR Apache-2.0
License:        MIT or ASL 2.0
URL:            https://crates.io/crates/mmtk
Source:         %{crates_source}
# Initial patched metadata
# bump versions of atomic, atomic-traits, crossbeam-dequeue, and enum-map
Patch0:         mmtk-fix-metadata.diff
# two OOB tests fail to panic
Patch1:         mmtk-0.9.0-disable-failed-tests.patch

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust-packaging

%global _description %{expand:
Framework for the design and implementation of high-performance and portable
memory managers.}

%description %{_description}

%package        devel
Summary:        %{summary}
BuildArch:      noarch

%description    devel %{_description}

This package contains library source intended for building other packages which
use the "%{crate}" crate.

%files          devel
%license COPYRIGHT LICENSE-APACHE LICENSE-MIT
%doc CHANGELOG.md CONTRIBUTING.md README.md
%{cargo_registry}/%{crate}-%{version_no_tilde}/

%package     -n %{name}+default-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+default-devel %{_description}

This package contains library source intended for building other packages which
use the "default" feature of the "%{crate}" crate.

%files       -n %{name}+default-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+analysis-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+analysis-devel %{_description}

This package contains library source intended for building other packages which
use the "analysis" feature of the "%{crate}" crate.

%files       -n %{name}+analysis-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+code_space-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+code_space-devel %{_description}

This package contains library source intended for building other packages which
use the "code_space" feature of the "%{crate}" crate.

%files       -n %{name}+code_space-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+extreme_assertions-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+extreme_assertions-devel %{_description}

This package contains library source intended for building other packages which
use the "extreme_assertions" feature of the "%{crate}" crate.

%files       -n %{name}+extreme_assertions-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+global_alloc_bit-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+global_alloc_bit-devel %{_description}

This package contains library source intended for building other packages which
use the "global_alloc_bit" feature of the "%{crate}" crate.

%files       -n %{name}+global_alloc_bit-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+hoard-sys-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+hoard-sys-devel %{_description}

This package contains library source intended for building other packages which
use the "hoard-sys" feature of the "%{crate}" crate.

%files       -n %{name}+hoard-sys-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+jemalloc-sys-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+jemalloc-sys-devel %{_description}

This package contains library source intended for building other packages which
use the "jemalloc-sys" feature of the "%{crate}" crate.

%files       -n %{name}+jemalloc-sys-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+malloc_hoard-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+malloc_hoard-devel %{_description}

This package contains library source intended for building other packages which
use the "malloc_hoard" feature of the "%{crate}" crate.

%files       -n %{name}+malloc_hoard-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+malloc_jemalloc-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+malloc_jemalloc-devel %{_description}

This package contains library source intended for building other packages which
use the "malloc_jemalloc" feature of the "%{crate}" crate.

%files       -n %{name}+malloc_jemalloc-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+malloc_mimalloc-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+malloc_mimalloc-devel %{_description}

This package contains library source intended for building other packages which
use the "malloc_mimalloc" feature of the "%{crate}" crate.

%files       -n %{name}+malloc_mimalloc-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+mimalloc-sys-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+mimalloc-sys-devel %{_description}

This package contains library source intended for building other packages which
use the "mimalloc-sys" feature of the "%{crate}" crate.

%files       -n %{name}+mimalloc-sys-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+nogc_lock_free-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+nogc_lock_free-devel %{_description}

This package contains library source intended for building other packages which
use the "nogc_lock_free" feature of the "%{crate}" crate.

%files       -n %{name}+nogc_lock_free-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+nogc_multi_space-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+nogc_multi_space-devel %{_description}

This package contains library source intended for building other packages which
use the "nogc_multi_space" feature of the "%{crate}" crate.

%files       -n %{name}+nogc_multi_space-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+nogc_no_zeroing-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+nogc_no_zeroing-devel %{_description}

This package contains library source intended for building other packages which
use the "nogc_no_zeroing" feature of the "%{crate}" crate.

%files       -n %{name}+nogc_no_zeroing-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+perf_counter-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+perf_counter-devel %{_description}

This package contains library source intended for building other packages which
use the "perf_counter" feature of the "%{crate}" crate.

%files       -n %{name}+perf_counter-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+pfm-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+pfm-devel %{_description}

This package contains library source intended for building other packages which
use the "pfm" feature of the "%{crate}" crate.

%files       -n %{name}+pfm-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+ro_space-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+ro_space-devel %{_description}

This package contains library source intended for building other packages which
use the "ro_space" feature of the "%{crate}" crate.

%files       -n %{name}+ro_space-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+sanity-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+sanity-devel %{_description}

This package contains library source intended for building other packages which
use the "sanity" feature of the "%{crate}" crate.

%files       -n %{name}+sanity-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+single_worker-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+single_worker-devel %{_description}

This package contains library source intended for building other packages which
use the "single_worker" feature of the "%{crate}" crate.

%files       -n %{name}+single_worker-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%package     -n %{name}+vm_space-devel
Summary:        %{summary}
BuildArch:      noarch

%description -n %{name}+vm_space-devel %{_description}

This package contains library source intended for building other packages which
use the "vm_space" feature of the "%{crate}" crate.

%files       -n %{name}+vm_space-devel
%ghost %{cargo_registry}/%{crate}-%{version_no_tilde}/Cargo.toml

%prep
%autosetup -n %{crate}-%{version_no_tilde} -p1
%cargo_prep

%generate_buildrequires
%cargo_generate_buildrequires

%build
%cargo_build

%install
%cargo_install

%if %{with check}
%check
%cargo_test
%endif

%changelog
%autochangelog
