Name:           racket
Version:        5.0.2
Release:        1%{?dist}
Epoch:          1
Summary:        A Scheme-based programming language and environment

Group:          Development/Languages
# libscheme is MIT-licensed
License:        LGPLv2 and MIT
URL:            http://racket-lang.org/
Source0:        http://download.racket-lang.org/installers/%{version}/racket/racket-%{version}-src-unix.tgz
Source1:        drracket.desktop
Source2:        racket.xml
#Patch0:         racket-5.0.2-multilib.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  cairo-devel
BuildRequires:  libGLU-devel
BuildRequires:  libXaw-devel
BuildRequires:  libXft-devel
BuildRequires:  libjpeg-devel
BuildRequires:  libpng-devel
BuildRequires:  desktop-file-utils

Provides:       plt-scheme = %{epoch}:%{version}-%{release}
Obsoletes:      plt-scheme < %{epoch}:%{version}-%{release}

# Racket bundles some forked system libraries; listing them here so
# they could be located if security problems are identified
#
# see src/foreign, src/racket and src/racket/src
#
# note: gc-7.1 is only used for bootstrapping; racket's own gc2 is used
# in installed system
#
# http://github.com/atgreen/libffi
# commit id: 3f5b1375ab1e2b8e3d59
Provides:       bundled(libffi) = 3.0.10rc0
# the bundled GMP is a mix of versions 3 and 4
Provides:       bundled(gmp) = 3.99
Provides:       bundled(lightning) = 1.2
Provides:       bundled(libunwind) = 0.99.0


%description
Racket, previously PLT Scheme, is an extensible, Scheme-based language
with an interactive mode that encourages experimentation, allowing the
composition of larger systems from small building blocks. It features
native-code JIT compilation and optional static typing.


%prep
%setup -q
# remove paths in collects/ that contain src directories;
# plot: a lot of bundled libs, and has been a security risk in the past
#  https://bugzilla.redhat.com/show_bug.cgi?id=529213
# mysterx: Windows-only
rm -rf collects/{plot,tests/mysterx}


%build
cd src
%configure --enable-shared
# Don't use rpath!
sed -i 's|/lib /usr/lib|/lib /usr/lib /lib64 /usr/lib64|' lt/libtool
make %{?_smp_mflags}


%install
rm -rf $RPM_BUILD_ROOT
cd src
# the collects files are compiled during the installation phase;
# relatively slow unless parallelized
make %{?_smp_mflags} install DESTDIR=$RPM_BUILD_ROOT

pushd $RPM_BUILD_ROOT%{_libdir}/racket/collects
# remove unsuitable files
# plot/src: just use the compiled version?
# rm -rf plot/src
# mysterx: Windows-only, needs VC++
# rm -rf tests/mysterx
popd


mkdir -p $RPM_BUILD_ROOT%{_datadir}/doc/racket-%{version}
for f in doc-license.txt \
    release-notes/COPYING.LIB \
    release-notes/COPYING-libscheme.txt;
do
    ln -s %{_datadir}/racket/doc/$f \
        $RPM_BUILD_ROOT%{_datadir}/doc/racket-%{version}
done

# Desktop entry
for d in $RPM_BUILD_ROOT%{_datadir}/pixmaps;
do
    mkdir -p $d
    ln -s %{_libdir}/racket/collects/icons/plt-logo-red-gradient.png \
        $d/plt-logo.png
done
desktop-file-install \
    --dir=$RPM_BUILD_ROOT%{_datadir}/applications \
    %{SOURCE1}

# MIME info for .rkt extension
for d in $RPM_BUILD_ROOT%{_datadir}/mime/packages;
do
    mkdir -p $d
    cp -p %{SOURCE2} $d
done

# remove .la files
rm -f $RPM_BUILD_ROOT%{_libdir}/lib*.la

# see IRC conversation with Sam Tobin-Hochstadt (samth)
# http://racket-lang.org/irc-logs/20101109.txt

# Racket uses the path to the source file as the unique identifier for
# the compiled objects it generates. As such it will include the
# $DESTDIR on Unix-style installs, though as the path is used only as
# an identifier and not an actual location on disk, this is harmless
# and is present in upstream's official Fedora packaging too
#
export QA_SKIP_BUILD_ROOT=1


%check
# currently this seems to stall
# src/racket/racket3m collects/tests/stepper/automatic-tests.rkt


%clean
rm -rf $RPM_BUILD_ROOT


%post
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :

%postun
update-mime-database %{_datadir}/mime &> /dev/null || :
update-desktop-database &> /dev/null || :


%files
%defattr(-,root,root,-)
%doc %{_datadir}/doc/racket-%{version}
%{_bindir}/*
%{_includedir}/racket
%{_libdir}/libracket3m*.so
%{_libdir}/libgracket*.so
%{_libdir}/racket
%{_mandir}/man1/*.1*
%{_datadir}/applications/drracket.desktop
%{_datadir}/mime/packages/racket.xml
%{_datadir}/pixmaps/plt-logo.png
%doc %{_datadir}/racket/doc


%changelog
* Wed Nov 10 2010 Michel Salim <salimma@fedoraproject.org> - 1:5.0.2-1
- Initial package

