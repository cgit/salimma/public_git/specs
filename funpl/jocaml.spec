%define opt %(test -x %{_bindir}/ocamlopt && echo 1 || echo 0)
%define debug_package %{nil}
#define _use_internal_dependency_generator 0

# Using these extracts internal modules even after
# %{_libdir}/jocaml has been cleaned up
#define __find_requires /usr/lib/rpm/ocaml-find-requires.sh
#define __find_provides /usr/lib/rpm/ocaml-find-provides.sh

Name:           jocaml
Version:        3.11.0
Release:        2%{?dist}
Summary:        Join-calculus extension of Objective Caml

Group:          Development/Libraries
License:        QPL and LGPLv2+ (with exceptions)
URL:            http://jocaml.inria.fr
Source0:        http://jocaml.inria.fr/pub/distri/jocaml-3.11/jocaml-%{version}.tar.gz
Source1:        http://jocaml.inria.fr/pub/distri/jocaml-3.11/jocaml-3.11-refman.html.tar.gz
Source2:        http://jocaml.inria.fr/pub/distri/jocaml-3.11/jocaml-3.11-refman.pdf

BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  ocaml = %{version}
Requires:       ocaml = %{version}

%description
JoCaml is a extension of Objective Caml for concurrent and
distributed programming, based upon the join-calculus.

This package comprises two batch compilers (a fast bytecode compiler
and an optimizing native-code compiler), an interactive toplevel system,
parsing tools (Lex,Yacc,Camlp4), a replay debugger, a documentation generator,
and a comprehensive library.


%package runtime
Group:          System Environment/Libraries
Summary:        JoCaml runtime environment
Requires:       util-linux-ng ocaml(runtime) = %{version}
Provides:       jocaml(runtime) = %{version}

%description runtime
JoCaml is a extension of Objective Caml for concurrent and
distributed programming, based upon the join-calculus.

This package contains the runtime environment needed to run JoCaml bytecode.


%package docs
Group:          Development/Languages
Summary:        Documentation for JoCaml
Requires:       jocaml = %{version}-%{release}


%description docs
JoCaml is a extension of Objective Caml for concurrent and
distributed programming, based upon the join-calculus.

This package contains documentation in PDF and HTML format.


%prep
%setup -q
%setup -q -T -D -a 1 -n %{name}-%{version}
cp -p %{SOURCE2} refman.pdf


%build
CFLAGS="$RPM_OPT_FLAGS" ./configure \
    -prefix %{_prefix} \
    -libdir %{_libdir}/jocaml \
    -mandir %{_mandir}

make world opt opt.opt


%install
rm -rf $RPM_BUILD_ROOT
make install \
     BINDIR=$RPM_BUILD_ROOT%{_bindir} \
     LIBDIR=$RPM_BUILD_ROOT%{_libdir}/jocaml
# fixes
pushd $RPM_BUILD_ROOT%{_bindir}
for i in ocaml*; do
  mv $i j$i
done
popd
# avoid duplicating Ocaml's libraries
pushd $RPM_BUILD_ROOT%{_libdir}/jocaml
mkdir safe
mv caml* *labels ex* *.conf* lib*run* libthreads* safe/
rm *.*
rm -rf caml*
mv safe/* .
rmdir safe
# rm stublibs/dllgraphics.so
rm stublibs/dllunix.so
popd


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/jocaml
%{_bindir}/jocamlc
%{_bindir}/jocamlc.opt
%{_bindir}/jocamlcp
%{_bindir}/jocamldep
%{_bindir}/jocamldep.opt
%{_bindir}/jocamllex
%{_bindir}/jocamllex.opt
%{_bindir}/jocamlmklib
%{_bindir}/jocamlmktop
%{_bindir}/jocamlopt
%{_bindir}/jocamlopt.opt
%{_bindir}/jocamlprof
%{_bindir}/jocamlyacc
%{_libdir}/jocaml/addlabels
%{_libdir}/jocaml/scrapelabels
%{_libdir}/jocaml/camlheader*
%{_libdir}/jocaml/expunge
%{_libdir}/jocaml/extract_crc
%{_libdir}/jocaml/ld.conf
%{_libdir}/jocaml/Makefile.config
%{_libdir}/jocaml/*.a
%{_libdir}/jocaml/vmthreads/*.mli
%{_libdir}/jocaml/vmthreads/*.a
%{_libdir}/jocaml/threads/*.mli
%{_libdir}/jocaml/threads/*.a
%{_libdir}/jocaml/threads/*.cmxa
%{_libdir}/jocaml/threads/*.cmx
%{_libdir}/ocaml/caml


%files runtime
%defattr(-,root,root,-)
%doc LICENSE README Changes_JoCaml
%{_bindir}/jocamlrun
%dir %{_libdir}/jocaml
%{_libdir}/jocaml/*.so
%{_libdir}/jocaml/stublibs
%dir %{_libdir}/jocaml/vmthreads
%{_libdir}/jocaml/vmthreads/*.cmi
%{_libdir}/jocaml/vmthreads/*.cma
%dir %{_libdir}/jocaml/threads
%{_libdir}/jocaml/threads/*.cmi
%{_libdir}/jocaml/threads/*.cma


%files docs
%defattr(-,root,root,-)
%doc refman.pdf htmlman


%changelog
* Tue Feb 24 2009 Michel Salim <salimma@fedoraproject.org> - 3.11.0-2
- Re-add caml* files, needed to pass test suite

* Mon Feb 23 2009 Michel Salim <salimma@fedoraproject.org> - 3.11.0-1
- Initial pacakge

