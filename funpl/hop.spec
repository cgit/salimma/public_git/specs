# find-debuginfo.sh cannot find Scheme source files
%global debug_package %{nil}

%global s2js_obsEVR 20110717-4

Name:           hop
Version:        2.3.0
Release:        1%{?dist}
Summary:        A web development kit

License:        GPLv2+
URL:            http://hop.inria.fr/
Source0:        ftp://ftp-sop.inria.fr/indes/fp/Hop/hop-%{version}.tar.gz

# Bigloo is not available on ppc64
ExcludeArch:    ppc64

BuildRequires:  bigloo
BuildRequires:  avahi-devel
BuildRequires:  openssl-devel
BuildRequires:  sqlite-devel
#Requires:       
# obsolete the last stand-alone scheme2js release
# not providing since there's no longer an exposed CLI for invoking
# scheme2js
# see
# http://fedoraproject.org/wiki/Packaging:Guidelines#Renaming.2FReplacing_Existing_Packages
# http://fedoraproject.org/wiki/Upgrade_paths_%E2%80%94_renaming_or_splitting_packages#Do_I_need_to_Provide_my_old_package_names.3F
Obsoletes:      scheme2js < %{s2js_obsEVR}

%description
Hop is a new programming language designed for the Web 2.0. It is a
higher-order language for programming interactive web applications
such as web agendas, web galleries, music players, etc. Hop can be
viewed as a replacement for traditional graphical toolkits. HOP is
implemented as a Web broker, i.e., a Web server that may act
indifferently as a regular Web server or Web proxy.

HOP features:

    * an extensive set of widgets for programming fancy GUIs.
    * an extensive set of libraries for:
          o handling database accesses.
          o dealing with network connections.
          o parsing wiki documents.
          o supporting various protocols such as IMAP, ICALENDAR, ...
          o supporting various formats such as EXIF, ID3, ...
          o parsing and generating XML documents.
          o ...


%prep
%setup -q -n %{name}-%{version}
# fix ChangeLog encoding
iconv -f ISO-8859-1 -t UTF-8 ChangeLog > ChangeLog.utf8
touch -r ChangeLog ChangeLog.utf8
mv ChangeLog.utf8 ChangeLog


%build
# optflags inherited from Bigloo
./configure \
  --prefix=%{_prefix} \
  --libdir=%{_libdir} \
  --mandir=%{_mandir} \
  --etcdir=%{_sysconfdir}
make %{?_smp_mflags}


%install
make install DESTDIR=$RPM_BUILD_ROOT
# remove 
# mark sofiles executable so they are detected by find-debuginfo.sh
# http://fedoraproject.org/wiki/Packaging:Debuginfo#Useless_or_incomplete_debuginfo_packages_due_to_packaging_issues
chmod a+x $RPM_BUILD_ROOT%{_libdir}/hop/*/*.so.*
# remove static archives
rm $RPM_BUILD_ROOT%{_libdir}/hop/*/*.a

# manually strip binaries and libraries, since find-debuginfo.sh is not run
strip $RPM_BUILD_ROOT%{_bindir}/* $RPM_BUILD_ROOT%{_libdir}/hop/*/lib*.so.*


%files
%doc LICENSE ChangeLog README
%{_bindir}/hop*
%{_libdir}/hop
%{_libdir}/libhop*.so.*
%{_libdir}/libscheme2js*.so.*
%{_datadir}/hop
%{_mandir}/man1/hop*.1*
%config(noreplace) %{_sysconfdir}/hop*rc.hop


%changelog
* Thu May  3 2012 Michel Salim <salimma@fedoraproject.org> - 2.3.0-1
- Update to final 2.3.0 release
- ExcludeArch ppc64, matching Bigloo
- Add BR on sqlite-devel
- %%prep: Explicitly specify encoding conversion target
- no longer redundantly clean buildroot in %%install
- Disable debuginfo generation for now

* Sat Apr  7 2012 Michel Salim <salimma@fedoraproject.org> - 2.3.0-0.rc2.1
- Initial package

