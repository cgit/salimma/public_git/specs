%define bzrdate 20090917

%define luaver 5.1
%define lualibdir %{_libdir}/lua/%{luaver}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}

Name:           lunatic-python
Version:        1.0.1
Release:        0.2.%{bzrdate}bzr%{?dist}
Summary:        Two-way bridge between Python and Lua

Group:          Development/Languages
License:        LGPLv2+
URL:            http://labix.org/lunatic-python
# bzr branch lp:~hircus/lunatic-python/trunk
# (cd lunatic-python && \
#  bzr export --root=lunatic-python ../lunatic-python.tar.bz2)
Source0:        %{name}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  python-devel lua-devel

%description
Lunatic Python is a two-way bridge between Python and Lua, allowing
these languages to intercommunicate. Being two-way means that it
allows Lua inside Python, Python inside Lua, Lua inside Python inside
Lua, Python inside Lua inside Python, and so on.


%prep
%setup -q -n %{name}


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python} setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
# Move Lua's python module to its correct place
mkdir -p $RPM_BUILD_ROOT%{lualibdir}
mv $RPM_BUILD_ROOT{%{python_sitearch}/python.so,%{lualibdir}}

 
%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc LICENSE 
%{lualibdir}/python.so
%{python_sitearch}/*


%changelog
* Wed Sep 23 2009 Michel Salim <salimma@fedoraproject.org> - 1.0.1-0.2.20090917bzr%{?dist}
- Follow guidelines for snapshot release tag

* Thu Sep 17 2009 Michel Salim <salimma@fedoraproject.org> - 1.0.1-0.1.bzr20090917%{?dist}
- Initial package

