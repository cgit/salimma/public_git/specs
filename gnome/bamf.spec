Summary:        Application matching framework
Name:           bamf
Version:        0.3.6
Release:        3%{?dist}
# Library bits are LGPLv2 or LGPLv3 (but not open-ended LGPLv2+);
# non-lib bits are GPLv3.
# pbrobinson points out that three files in the lib are actually
# marked GPL in headers, making library GPL, though we think this
# may not be upstream's intention. For now, marking library as
# GPL.
# License:      LGPLv2 or LGPLv3
License:        GPLv2 or GPLv3
Group:          System Environment/Libraries
URL:            https://launchpad.net/bamf
Source0:        http://launchpad.net/bamf/0.3/%{version}/+download/%{name}-%{version}.tar.gz
# backport of commit 525:
# move directives from CFLAGS to scanner flags
Patch0:         bamf-0.3.6-use-scanner-flags.patch
# Fix test
Patch1:		bamf-0.3.6-glib-fix.patch

BuildRequires:  vala-tools
BuildRequires:  gtk-doc
BuildRequires:  gobject-introspection-devel
BuildRequires:  pkgconfig(dbus-glib-1)
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gdk-2.0)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(gdk-x11-2.0)
BuildRequires:  pkgconfig(gdk-x11-3.0)
BuildRequires:  pkgconfig(gtk+-2.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(libgtop-2.0)
BuildRequires:  pkgconfig(libwnck-1.0)
BuildRequires:  pkgconfig(libwnck-3.0)
BuildRequires:  pkgconfig(x11)

%description
BAMF removes the headache of applications matching into a simple DBus
daemon and C wrapper library. Currently features application matching
at amazing levels of accuracy (covering nearly every corner case). This
package contains the bamf library built against GTK+ 2.

%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
License:        GPLv2 or GPLv3
Requires:       %{name} = %{version}-%{release}
# For %{_libdir}/girepository-1.0 and %{_datadir}/gir-1.0
#Requires:      gobject-introspection-devel
Requires:       pkgconfig

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        daemon
Summary:        Application matching framework
Group:          System Environment/Libraries
License:        GPLv3

%description    daemon
BAMF removes the headache of applications matching into a simple DBus
daemon and C wrapper library. Currently features application matching
at amazing levels of accuracy (covering nearly every corner case). This
package contains the bamf daemon and supporting data.

%package -n     %{name}3
Summary:        Application matching framework (GTK+ 3 build)

%description -n %{name}3
BAMF removes the headache of applications matching into a simple DBus
daemon and C wrapper library. Currently features application matching
at amazing levels of accuracy (covering nearly every corner case). This
package contains the bamf library built against GTK+ 3.

%package -n     %{name}3-devel
Summary:        Development files for %{name} (GTK+ 3 build)
Group:          Development/Libraries
License:        GPLv2 or GPLv3
Requires:       %{name}3 = %{version}-%{release}
# For %{_libdir}/girepository-1.0 and %{_datadir}/gir-1.0
#Requires:      gobject-introspection-devel
Requires:       pkgconfig

%description -n %{name}3-devel
The %{name}3-devel package contains libraries and header files for
developing applications that use %{name} (GTK+ 3 build).


%prep
%setup -q
%patch0 -p1 -b .use-scanner-flags
%patch1 -p1 -b .glibfix

%build
%global _configure ../configure
rm -rf build-gtk3 build-gtk2
mkdir build-gtk3 build-gtk2
pushd build-gtk2
%configure --disable-static --disable-webapps --with-gtk=2 --enable-gtk-doc
make %{?_smp_mflags} VALA_API_GEN="/usr/bin/vapigen-0.20"
popd
pushd build-gtk3
%configure --disable-static --disable-webapps --with-gtk=3 --enable-gtk-doc
make %{?_smp_mflags} VALA_API_GEN="/usr/bin/vapigen-0.20"
popd

%install
make -C build-gtk2 DESTDIR=%{buildroot} install
make -C build-gtk3 DESTDIR=%{buildroot} install

find %{buildroot} -regex ".*\.la$" | xargs rm -f --

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%clean
rm -rf %{name}-%{version} %{name}-%{version}-gtk3

%files
%doc COPYING.LGPL COPYING
%{_libdir}/libbamf.so.*

%files -n       %{name}3
%doc COPYING.LGPL COPYING
%{_libdir}/libbamf3.so.*

%files          devel
%doc COPYING.LGPL COPYING
%{_includedir}/libbamf
%{_libdir}/libbamf.so
%{_libdir}/pkgconfig/libbamf.pc
%{_datadir}/gtk-doc
%{_libdir}/girepository-1.0/Bamf*.typelib
%{_datadir}/gir-1.0/Bamf*.gir
%dir %{_datadir}/vala
%dir %{_datadir}/vala/vapi
%{_datadir}/vala/vapi/libbamf.vapi

%files -n       %{name}3-devel
%doc COPYING.LGPL COPYING
%{_includedir}/libbamf3
%{_libdir}/libbamf3.so
%{_libdir}/pkgconfig/libbamf3.pc
%{_datadir}/gtk-doc
%{_libdir}/girepository-1.0/Bamf*.typelib
%{_datadir}/gir-1.0/Bamf*.gir
%dir %{_datadir}/vala
%dir %{_datadir}/vala/vapi
%{_datadir}/vala/vapi/libbamf3.vapi

%files          daemon
%doc COPYING
%{_libexecdir}/bamfdaemon
%{_datadir}/dbus-1/services/*.service

%changelog
* Thu Jun 13 2013 Michel Salim <salimma@fedoraproject.org> - 0.3.6-3
- Spec clean-ups

* Thu Apr 25 2013 Tom Callaway <spot@fedoraproject.org> - 0.3.6-2
- fix build

* Sat Apr 20 2013 Michel Salim <salimma@fedoraproject.org> - 0.3.6-1
- Update to 0.3.6

* Wed Jul 18 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.104-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Jan 12 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.104-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Nov 10 2011 Adam Williamson <awilliam@redhat.com> - 0.2.104-1
- new version, adjust for various upstream fixes
- rebuild against new libpng
- build both gtk+2 and gtk+3 libs, and package separately (like Ubuntu)

* Wed May 25 2011 Adam Williamson <awilliam@redhat.com> - 0.2.90-2
- don't depend on gtk-doc but own /usr/share/gtk-doc instead (#707545)

* Wed May 11 2011 Adam Williamson <awilliam@redhat.com> - 0.2.90-1
- new release 0.2.90

* Wed Mar 23 2011 Adam Williamson <awilliam@redhat.com> - 0.2.80-1
- new release 0.2.80

* Mon Mar 07 2011 Adam Williamson <awilliam@redhat.com> - 0.2.78-1
- new release 0.2.78

* Mon Feb 07 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.2.74-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Sun Jan 23 2011 Adam Williamson <awilliam@redhat.com> - 0.2.74-1
- new release 0.2.74
- update license of library (thanks pbrobinson)
- fix build by disabling a strict warning which seems to have showed
  up in gcc-4.6.0-0.3
- gir and vala devel files aren't getting installed any more

* Fri Dec 03 2010 Adam Williamson <awilliam@redhat.com> - 0.2.64-1
- initial package
