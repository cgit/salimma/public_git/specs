%{!?python_sitearch: %global python_sitearch %(%{__python}26 -c "from distutils.sysconfig import get_python_lib; print (get_python_lib(1))")}

# fix byte-compilation to not use default python
# http://fedoraproject.org/wiki/Python26#Byte_compiling
%global __os_install_post %__multiple_python_os_install_post

%global short_name psutil

# Filter Python modules from Provides
%{?filter_setup:
%filter_provides_in %{python_sitearch}/.*\.so$
%filter_setup
}

Name:           python26-psutil
Version:        0.6.1
Release:        2%{?dist}
Summary:        A process and system utilities module for Python

Group:          Development/Languages
License:        BSD
URL:            http://psutil.googlecode.com/
Source0:        http://psutil.googlecode.com/files/%{short_name}-%{version}.tar.gz

BuildRequires:  python26-devel

BuildRoot:      %(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

%description
psutil is a module providing an interface for retrieving information on all
running processes and system utilization (CPU, memory, disks, network, users) in
a portable way by using Python, implementing many functionalities offered by
command line tools such as: ps, top, df, kill, free, lsof, free, netstat,
ifconfig, nice, ionice, iostat, iotop, uptime, pidof, tty, who, taskset, pmap.


%prep
%setup -q -n %{short_name}-%{version}

# Remove shebangs
for file in psutil/*.py; do
  sed -i.orig -e 1d $file && \
  touch -r $file.orig $file && \
  rm $file.orig
done


%build
CFLAGS=$RPM_OPT_FLAGS %{__python}26 setup.py build


%install
rm -rf $RPM_BUILD_ROOT
%{__python}26 setup.py install \
  --skip-build \
  --root $RPM_BUILD_ROOT

# Fix permissions
chmod 0755 $RPM_BUILD_ROOT%{python_sitearch}/*.so


%clean
rm -rf $RPM_BUILD_ROOT


%files
%doc CREDITS HISTORY LICENSE README
%{python_sitearch}/%{short_name}/
%{python_sitearch}/*.egg-info
%{python_sitearch}/*.so


%changelog
* Mon Apr  8 2013 Michel Salim <salimma@fedoraproject.org> - 0.6.1-2
- Byte-compile with the correct Python version
- Clean up buildroot

* Tue Mar 19 2013 Michel Salim <salimma@fedoraproject.org> - 0.6.1-1
- Initial package, based on python-psutil-0.6.1-1
