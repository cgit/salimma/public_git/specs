%global srcname oolite-dev-source

Name:           oolite
Version:        1.74.2
Release:        1%{?dist}
Summary:        A space simulation game, inspired by Elite

Group:          Amusements/Games
License:        GPLv2+
URL:            http://oolite.org/
Source0:        http://download.berlios.de/oolite-linux/%{srcname}-%{version}.tar.bz2
# Use RPM_OPT_FLAGS instead of default compiler flags
Patch0:         %{srcname}-1.74.2-rpmoptflags.patch
# explicitly link against pthread instead of inheriting it
Patch1:         %{srcname}-1.74.2-implicit-dso.patch
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  gcc-objc
BuildRequires:  gnustep-base-devel desktop-file-utils
BuildRequires:  mesa-libGLU-devel SDL_image-devel SDL_mixer-devel
BuildRequires:  libpng-devel espeak-devel
# https://fedoraproject.org/wiki/Packaging:No_Bundled_Libraries#Requirement_if_you_bundle
# js is bundled because oolite needs a non-threadsafe, UTF-8 js while
# Fedora's is compiled threadsafe and, in the past, without UTF-8 support
Provides:       bundled(js) = 1.70

%description
Oolite is a space simulation game, inspired by Elite, powered by
Objective-C and OpenGL, and designed as a small game that is easy for
users to pick up, modify and expand upon. Almost every aspect of the
game can be changed by using simple, free graphics packages and text
editors.


%package        doc
Summary:        Documentation for %{name}
Group:          Documentation
License:        GPLv2+
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}

%description    doc
Oolite is a space sim game, inspired by Elite, powered by Objective-C
and OpenGL, and designed as a small game that is easy for users to
pick up, modify and expand upon. Almost every aspect of the game can
be changed by using simple, free graphics packages and text editors.

This package contains documentation in PDF format.


%prep
%setup -q -n %{name}-dev-source-%{version}
%patch0 -p1 -b .rpmoptflags
%patch1 -p1 -b .implicit-dso

# Fix end-of-line encodings
for f in Doc/contributors.txt Doc/CHANGELOG.TXT; do
    touch -r $f _tstamp.txt
    sed -i 's|\r||' $f
    touch -r _tstamp.txt $f
done
rm _tstamp.txt

# Fix desktop file
sed -i installers/FreeDesktop/oolite.desktop \
    -e 's|Exec=oolite|Exec=openapp oolite|'


%build
. %{_libdir}/GNUstep/Makefiles/GNUstep.sh

(
  cd deps/Cross-platform-deps/SpiderMonkey/js/src &&
  make -f Makefile.ref XCFLAGS="$RPM_OPT_FLAGS" &&
  ln -s Linux_All_{DBG,OPT}.OBJ
)

# Not parallel-buildable
make %{?_smp_mflags} debug=yes messages=yes CP="cp -p"

# GNUmakefile.postamble tags on the .dbg extension
mv oolite.app/oolite{.dbg,}


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_libdir}/GNUstep/Applications
mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps
cp -pr oolite.app $RPM_BUILD_ROOT%{_libdir}/GNUstep/Applications/
cp -p installers/FreeDesktop/oolite-icon.png $RPM_BUILD_ROOT%{_datadir}/pixmaps/
desktop-file-install \
  --dir=$RPM_BUILD_ROOT%{_datadir}/applications \
  --remove-category=Application \
  --add-category=Simulation \
  installers/FreeDesktop/oolite.desktop

# remove docs that we don't want to package
# to simplify %%files declaration
rm Doc/{PORTING,README_LINUX}.TXT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%doc Doc/contributors.txt Doc/*.TXT
%{_libdir}/GNUstep/Applications/oolite.app
%{_datadir}/applications/oolite.desktop
%{_datadir}/pixmaps/oolite-icon.png


%files doc
%defattr(-,root,root,-)
%doc Doc/*.pdf


%changelog
* Wed Nov 10 2010 Michel Salim <salimma@fedoraproject.org> - 1.74.2-1
- Update to 1.74.2

* Thu Sep 24 2009 Michel Salim <salimma@fedoraproject.org> - 1.73.4-1
- Update to 1.73.4
- Adjust file locations to use flattened layout
- Use Fedora-specific compiler flags
- Build with verbose output

* Mon Aug 11 2008 Michel Salim <salimma@fedoraproject.org> - 1.65-1
- Initial package

