Name:           budgie-desktop
Version:        8
Release:        2%{?dist}
Summary:        An elegant desktop with GNOME integration

License:        GPLv2+
URL:            https://github.com/evolve-os/budgie-desktop
Source0:        https://build.opensuse.org/source/home:ikeydoherty:evolve/budgie-desktop/budgie-desktop-%{version}.tar.xz
# Propagate configuration parameters to budgie-1.0.pc.in
# https://github.com/evolve-os/budgie-desktop/pull/124
Patch0:         budgie-desktop-8-fix-pkgconfig.patch

BuildRequires:  vala >= 0.22
BuildRequires:  pkgconfig(gobject-2.0) >= 2.38.0
BuildRequires:  pkgconfig(gio-2.0) >= 2.38.0
BuildRequires:  pkgconfig(gtk+-3.0) >= 3.10.0
BuildRequires:  pkgconfig(libpulse) >= 2.0
BuildRequires:  pkgconfig(libpulse-mainloop-glib)
BuildRequires:  pkgconfig(libmutter) >= 3.10.1
BuildRequires:  pkgconfig(libwnck-3.0) >= 3.4.7
BuildRequires:  pkgconfig(upower-glib) >= 0.9.20
BuildRequires:  pkgconfig(libgnome-menu-3.0) >= 3.10.1
BuildRequires:  pkgconfig(libpeas-gtk-1.0) >= 1.8.0
BuildRequires:  pkgconfig(gio-unix-2.0) >= 2.38.0
BuildRequires:  pkgconfig(gee-0.8)
BuildRequires:  chrpath

BuildRequires:  desktop-file-utils

%description
Budgie is the flagship desktop of the Evolve OS Linux Distribution,
and is an Evolve OS project. Designed with the modern user in mind, it
focuses on simplicity and elegance.

The Budgie Desktop tightly integrates with the GNOME stack, employing
underlying technologies to offer an alternative desktop experience. In
the spirit of open source, the project is compatible with and
available for other Linux distributions.


%package devel
Summary:        Development files for the Budgie Desktop
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
Development files for the Budgie Desktop


%prep
%setup -q
%patch0 -p1 -b .fix-pkgconfig


%build
%configure --disable-silent-rules
make %{?_smp_mflags}


%install
%make_install pkgconfigdir=%{_libdir}/pkgconfig
find %{buildroot} -name '*.la' -delete
# Remove RPATHs
chrpath --delete %{buildroot}%{_libdir}/*.so
chrpath --delete %{buildroot}%{_bindir}/*
chrpath --delete %{buildroot}%{_libdir}/budgie-desktop/*.so

# Validate installed desktop file
desktop-file-validate %{buildroot}/%{_datadir}/xsessions/budgie-desktop.desktop


%post -p /sbin/ldconfig

%postun
/sbin/ldconfig
if [ $1 -eq 0 ] ; then
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :
fi

%posttrans
    /usr/bin/glib-compile-schemas %{_datadir}/glib-2.0/schemas &> /dev/null || :


%files
%doc LICENSE README.md
%{_libdir}/lib*.so.*
%{_bindir}/budgie-*
%dir %{_libdir}/budgie-desktop
%{_libdir}/budgie-desktop/Gvc-1.0.typelib
%{_libdir}/budgie-desktop/*.plugin
%{_libdir}/budgie-desktop/*.so
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/Budgie*.typelib
%dir %{_datadir}/budgie-desktop
%{_datadir}/budgie-desktop/layout.ini
%{_datadir}/xsessions/budgie-desktop.desktop
%{_datadir}/glib-2.0/schemas/com.evolve-os.budgie.panel.gschema.xml
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/Budgie-1.0.gir

%files devel
%{_includedir}/BudgiePlugin.h
%{_libdir}/pkgconfig/budgie*.pc
%{_libdir}/lib*.so
%dir %{_datadir}/vala
%dir %{_datadir}/vala/vapi
%{_datadir}/vala/vapi/budgie-1.0.*


%changelog
* Sun Dec  7 2014 Michel Alexandre Salim <salimma@fedoraproject.org> - 8-2
- Propagate configuration parameters to budgie-1.0.pc.in
- Validate the Budgie desktop session file
- Verbose build output
- Fix directory ownerships

* Fri Dec  5 2014 Michel Alexandre Salim <salimma@fedoraproject.org> - 8-1
- Initial package
