# tests cannot be executed by the build user
# since they require access to /etc/trytond.conf
# which is limited to users in the tryton group
# for security reasons
#
# packagers should run the tests by hand to make sure
# they still pass, ideally *inside* the mock used for
# building gnuhealth:
# 1. mock -r ... --with check /path/to/gnuhealth-*.src.rpm
#    this will fail as /etc/trytond.conf is not readable
# 2. mock -r ... shell
# 3. as the mock root user, rpmbuild -bb /builddir/build/SPECS/gnuhealth.spec
#    (mock already cached the --with setting in ~/.rpmmacros)
%bcond_with check

Name:           gnuhealth
# latest version for Tryton 2.2.x
# update to 1.6.1 when Tryton 2.4.x lands
Version:        1.4.5
Release:        1%{?dist}
Summary:        The free Health and Hospital Information System

License:        GPLv3+
URL:            http://health.gnu.org/ 
Source0:        http://ftp.gnu.org/gnu/health/gnuhealth-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python-qrcode
BuildRequires:  python-setuptools
BuildRequires:  trytond
BuildRequires:  trytond-account-invoice
BuildRequires:  trytond-calendar
BuildRequires:  trytond-company
BuildRequires:  trytond-country
BuildRequires:  trytond-currency
BuildRequires:  trytond-party
BuildRequires:  trytond-product

Requires:	python-qrcode
Requires:       trytond
Requires:       trytond-account-invoice
Requires:       trytond-calendar
Requires:       trytond-company
Requires:       trytond-country
Requires:       trytond-currency
Requires:       trytond-party
Requires:       trytond-product


%description
GNU HEALTH is a free, centralized Health and Hospital Information
System that provides the following functionality :

    * Electronic Medical Record (EMR)
    * Hospital Information System (HIS)
    * Health Information System

The goal is to collaborate with health professionals around the world
to improve the quality of life of the underprivileged, providing a
free system that optimizes health promotion and disease prevention.


%prep
%setup -q
# 1.6.1: does not come with setup.py yet
# mv {,off-}health_services


%build
for mod in health health_*;
do
    pushd $mod
    %{__python} setup.py build
    popd
done


%install
for mod in health health_*;
do
    pushd $mod
    %{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
    popd
done

mkdir -p %{buildroot}%{_mandir}/man1/
cp -p health/man/gnuhealth.1 %{buildroot}%{_mandir}/man1/

# make test cases executable
find %{buildroot}%{python_sitelib}/trytond/modules/health*/tests/test*.py \
     ! -perm -a+x -exec chmod +x "{}" \;

# 1.6.1
#mv {off-,}health_services
#cp -pr health_services %{buildroot}%{python_sitelib}/trytond/modules/


%if %{with check}
%check
export PYTHONPATH=$PYTHONPATH:%{buildroot}%{python_sitelib}
# some modules don't actually have tests
for mod in $(find health*/tests/__init__.py -type f \
    | sort \
    | sed -e 's|/tests/__init__.py$||');
do
    pushd $mod
    %{__python} setup.py test
    popd
done
%endif


%files
%doc COPYING README
%{_mandir}/man1/gnuhealth.1*
%{python_sitelib}/trytond/modules/health*
%{python_sitelib}/trytond_health*-%{version}-py*.egg*


%changelog
* Sat Jun  2 2012 Michel Salim <salimma@fedoraproject.org> - 1.4.5-1
- Initial package
