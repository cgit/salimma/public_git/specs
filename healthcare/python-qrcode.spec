%global pkgname qrcode

Name:           python-%{pkgname}
Version:        2.4.1
Release:        2%{?dist}
Summary:        Python QR Code image generator

License:        BSD
URL:            https://github.com/lincolnloop/python-qrcode
Source0:        http://pypi.python.org/packages/source/q/qrcode/qrcode-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python-devel
BuildRequires:  python-imaging
Requires:       python-imaging

%description
This module uses the Python Imaging Library (PIL) to allow for the
generation of QR Codes.


%prep
%setup -q -n %{pkgname}-%{version}


%build
%{__python} setup.py build


%install
%{__python} setup.py install -O1 --skip-build --root $RPM_BUILD_ROOT
%{__mv} $RPM_BUILD_ROOT%{_bindir}/qr{,code}


%check
# in lieue of a real test suite
for m in $(find qrcode -name '*.py' \
    | grep -v __init__ \
    | sort \
    | sed -e 's|/|.|g' \
    | sed -e 's|.py$||g');
do
    %{__python} -c "import $m"
done


%files
%doc LICENSE README.rst CHANGES.rst
%{python_sitelib}/*
%{_bindir}/qrcode


%changelog
* Wed Jun  6 2012 Michel Salim <salimma@fedoraproject.org> - 2.4.1-2
- Clean up spec, removing unnecessary declarations
- Rename tool in %%{_bindir} to the less ambiguous qrcode

* Sat Jun  2 2012 Michel Salim <salimma@fedoraproject.org> - 2.4.1-1
- Initial package
