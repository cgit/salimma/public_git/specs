Name:           kristall
Version:        0.3
Release:        1%{?dist}
Summary:        A high-quality visual cross-platform gemini browser

License:        GPLv3+
URL:            https://kristall.random-projects.net/
Source0:        https://github.com/MasterQ32/kristall/archive/V%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires:  desktop-file-utils
BuildRequires:  gcc-c++
BuildRequires:  openssl-devel
BuildRequires:  qt5-qtbase-devel
BuildRequires:  qt5-qtmultimedia-devel
BuildRequires:  qt5-qtsvg-devel
#Requires:       

%description
Kristall is a browser without support for css/js/wasm or graphical websites. It
can display user-styled documents in several formats, including gemini, html,
markdown, … provided by a server via gemini, gopher, http, finger, …


%prep
%autosetup


%build
mkdir -p build
cd build
%qmake_qt5 ../src/kristall.pro
%make_build


%install
cp -p build/kristall .
for d in \
  %{_bindir} \
  %{_datadir}/applications \
  %{_datadir}/icons/hicolor/{16x16,32x32,64x64,128x128,scalable}/apps \
; do
  mkdir -p %{buildroot}$d
done
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps
%make_install PREFIX=%{_prefix}


%check
desktop-file-validate \
  %{buildroot}%{_datadir}/applications/Kristall.desktop


%files
%license LICENSE
%doc README.md ROADMAP.md
%{_bindir}/kristall
%{_datadir}/applications/Kristall.desktop
%{_datadir}/icons/hicolor/*/apps/net.random-projects.kristall.*


%changelog
* Tue Aug 18 2020 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.3-1
- Initial Fedora package
