Name:           xml2
Version:        0.5
Release:        1%{?dist}
Summary:        Convert between XML, HTML, CSV and a line-oriented format

License:        GPLv2+
URL:            http://ofb.net/~egnor/xml2/
Source0:        http://download.ofb.net/gale/xml2-0.5.tar.gz

BuildRequires:  pkgconfig(libxml-2.0)
#Requires:       

%description
xml2 tools are used to convert XML, HTML and CSV to and from a
line-oriented format more amenable to processing by classic Unix
pipeline processing tools, like grep, sed, awk, cut, shell scripts,
and so forth.


%prep
%setup -q


%build
%configure
make %{?_smp_mflags}


%install
%make_install
# fix absolute symlinks
pushd $RPM_BUILD_ROOT%{_bindir}
ln -sf 2xml 2html
ln -sf xml2 html2
popd


%files
%doc COPYING
%{_bindir}/2csv
%{_bindir}/2html
%{_bindir}/2xml
%{_bindir}/csv2
%{_bindir}/html2
%{_bindir}/xml2


%changelog
* Wed Oct  8 2014 Michel Alexandre Salim <salimma@fedoraproject.org> - 0.5-1
- Initial package
