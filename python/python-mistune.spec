%global srcname mistune

Name:           python-%{srcname}
Version:        2.0.0
Release:        %autorelease
Summary:        A sane Markdown parser with useful plugins and renderers
License:        BSD
URL:            https://github.com/lepture/mistune
Source0:        %{pypi_source %{srcname}}

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools
# since upstream's pyproject.toml is too incomplete for auto BRs
BuildRequires:  python3dist(pip)
BuildRequires:  python3dist(wheel)
# Test dependencies:
BuildRequires:  python3dist(pytest)

%global _description %{expand:
A fast yet powerful Python Markdown parser with renderers and plugins.}

%description %{_description}


%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}

%description -n python%{python3_pkgversion}-%{srcname} %{_description}


%prep
%autosetup -p1 -n %{srcname}-%{version}


%generate_buildrequires
# project's pyproject.toml caused our script to choke
# %%pyproject_buildrequires


%build
%pyproject_wheel


%install
%pyproject_install
%pyproject_save_files %%{srcname}


%check
%pytest


%files -n python%{python3_pkgversion}-%{srcname} -f %{pyproject_files}
%license LICENSE
%doc README.rst


%changelog
%autochangelog
