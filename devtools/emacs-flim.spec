%global pkg flim
%global pkgname FLIM

Name:           emacs-%{pkg}
Version:        1.14.9
Release:        3%{?dist}
Summary:        Basic library for handling email messages for Emacs

Group:          Applications/Editors
License:        GPLv2+
URL:            http://www.kanji.zinbun.kyoto-u.ac.jp/~tomo/elisp/FLIM/
Source0:        http://www.kanji.zinbun.kyoto-u.ac.jp/~tomo/comp/emacsen/lisp/flim/flim-1.14/%{pkg}-%{version}.tar.gz

Provides:       flim = %{version}-%{release}
Obsoletes:      flim < %{version}-%{release}

BuildArch:      noarch
BuildRequires:  emacs
Requires:       emacs(bin) >= %{_emacs_version}

%description
%{pkgname} is an add-on package for GNU Emacs. It provides basic
 features for message representation and encoding.

%package -n %{name}-el
Summary:        Elisp source files for %{pkgname} under GNU Emacs
Requires:       %{name} = %{version}-%{release}

%description -n %{name}-el
This package contains the elisp source files for %{pkgname} under GNU Emacs. You
do not need to install this package to run %{pkgname}. Install the %{name}
package to use %{pkgname} with GNU Emacs.

%prep
%setup -q -n %{pkg}-%{version}

%build
rm -f mailcap*
make LISPDIR=$RPM_BUILD_ROOT%{_emacs_sitelispdir}


%install
rm -rf $RPM_BUILD_ROOT
%makeinstall PREFIX=$RPM_BUILD_ROOT%{_prefix} \
             LISPDIR=$RPM_BUILD_ROOT%{_emacs_sitelispdir}


%files
%defattr(-,root,root,-)
%doc FLIM-API.en README.en README.ja
%{_emacs_sitelispdir}/%{pkg}/*.elc
%dir %{_emacs_sitelispdir}/%{pkg}

%files -n %{name}-el
%defattr(-,root,root,-)
%{_emacs_sitelispdir}/%{pkg}/*.el

%changelog
* Wed Mar 30 2011 Michel Salim <salimma@fedoraproject.org> - 1.14.9-3
- Rename package to meet packaging guidelines (# 658338)
- Disable xemacs subpackage; not supported by emacs-apel anymore

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.14.9-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Mon Nov 29 2010 Jens Petersen <petersen@redhat.com> - 1.14.9-1
- update to 1.14.9
- flim-xemacs-batch-autoloads.patch no longer needed

* Fri Jul 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.14.8-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Tue Feb 24 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.14.8-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Mon Sep 24 2007 Jens Petersen <petersen@redhat.com> - 1.14.8-3
- update license to GPLv2+

* Mon Sep 25 2006 Jens Petersen <petersen@redhat.com> - 1.14.8-2
- update url and source location

* Fri Aug  4 2006 Jens Petersen <petersen@redhat.com> - 1.14.8-1
- update to 1.14.8
- add flim-xemacs-batch-autoloads.patch to fix generation of autoloads
  for xemacs-21.5

* Mon May 30 2005 Jens Petersen <petersen@redhat.com> - 1.14.7-3
- Initial import into Extras
- restore xemacs subpackage

* Wed Feb 23 2005 Elliot Lee <sopwith@redhat.com> 1.14.7-2
- Remove xemacs subpackage

* Sat Oct  9 2004 Jens Petersen <petersen@redhat.com> - 1.14.7-1
- update to 1.14.7 release
- flim-1.14.6-mel-u-tempfile.patch no longer needed

* Wed Oct  6 2004 Jens Petersen <petersen@redhat.com> - 1.14.6-3
- drop requirements on emacs/xemacs for -nox users
  (Lars Hupfeldt Nielsen, 134479)

* Tue Sep 28 2004 Warren Togami <wtogami@redhat.com> - 1.14.6-2
- remove redundant docs, large changelog, tests

* Thu May 20 2004 Jens Petersen <petersen@redhat.com> - 1.14.6-1
- update to 1.14.6
- add flim-1.14.6-mel-u-tempfile.patch to fix CAN-2004-0422
- move redundant %%emacsver and %%xemacsver into requirements
- better url and summary

* Fri Feb 13 2004 Elliot Lee <sopwith@redhat.com> - 1.14.5-2
- rebuilt

* Sat Aug  2 2003 Jens Petersen <petersen@redhat.com> - 1.14.5-1
- update to 1.14.5

* Wed Jan 22 2003 Tim Powers <timp@redhat.com>
- rebuilt

* Tue Dec 24 2002 Jens Petersen <petersen@redhat.com> 1.14.4-1
- update to 1.14.4
- install xemacs package under datadir
- own emacs site-lisp and down
- own xemacs-packages and down

* Wed Nov 20 2002 Tim Powers <timp@redhat.com>
- rebuild in current collinst

* Thu Jul 18 2002 Akira TAGOH <tagoh@redhat.com> 1.14.3-7
- add the owned directory.

* Fri Jun 21 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Thu May 23 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Fri Feb 22 2002 Tim Powers <timp@redhat.com>
- rebuilt in new environment

* Wed Jan 09 2002 Tim Powers <timp@redhat.com>
- automated rebuild

* Mon Nov 30 2001 Jens Petersen <petersen@redhat.com> 1.14.3-2
- noarch, since xemacs available on ia64 
- require apel

* Fri Oct 26 2001 Akira TAGOH <tagoh@redhat.com> 1.14.3-1
- Initial release.
  Separated from semi package.
