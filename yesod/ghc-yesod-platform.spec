# https://fedoraproject.org/wiki/Packaging:Haskell
# https://fedoraproject.org/wiki/PackagingDrafts/Haskell

%global pkg_name yesod-platform

%global common_summary Meta package for Yesod

%global common_description Instead of allowing version ranges of dependencies, this package requires specific versions to avoid dependency hell

Name:           ghc-%{pkg_name}
Version:        1.1.8
Release:        1%{?dist}
Summary:        %{common_summary}

License:        MIT
URL:            http://hackage.haskell.org/package/%{pkg_name}
Source0:        http://hackage.haskell.org/packages/archive/%{pkg_name}/%{version}/%{pkg_name}-%{version}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  ghc-ReadArgs-devel
BuildRequires:  ghc-SHA-devel
BuildRequires:  ghc-aeson-devel
BuildRequires:  ghc-ansi-terminal-devel
BuildRequires:  ghc-asn1-data-devel
BuildRequires:  ghc-attoparsec-devel
BuildRequires:  ghc-attoparsec-conduit-devel
BuildRequires:  ghc-authenticate-devel
BuildRequires:  ghc-base-unicode-symbols-devel
BuildRequires:  ghc-base64-bytestring-devel
BuildRequires:  ghc-base64-conduit-devel
BuildRequires:  ghc-basic-prelude-devel
BuildRequires:  ghc-blaze-builder-devel
BuildRequires:  ghc-blaze-builder-conduit-devel
BuildRequires:  ghc-blaze-html-devel
BuildRequires:  ghc-blaze-markup-devel
BuildRequires:  ghc-byteorder-devel
BuildRequires:  ghc-case-insensitive-devel
BuildRequires:  ghc-cereal-devel
BuildRequires:  ghc-certificate-devel
BuildRequires:  ghc-cipher-aes-devel
BuildRequires:  ghc-cipher-rc4-devel
BuildRequires:  ghc-classy-prelude-devel
BuildRequires:  ghc-clientsession-devel
BuildRequires:  ghc-conduit-devel
BuildRequires:  ghc-cookie-devel
BuildRequires:  ghc-cprng-aes-devel
BuildRequires:  ghc-crypto-api-devel
BuildRequires:  ghc-crypto-conduit-devel
BuildRequires:  ghc-crypto-numbers-devel
BuildRequires:  ghc-crypto-pubkey-devel
BuildRequires:  ghc-crypto-pubkey-types-devel
BuildRequires:  ghc-crypto-random-api-devel
BuildRequires:  ghc-cryptohash-devel
BuildRequires:  ghc-css-text-devel
BuildRequires:  ghc-data-default-devel
BuildRequires:  ghc-date-cache-devel
BuildRequires:  ghc-dlist-devel
BuildRequires:  ghc-email-validate-devel
BuildRequires:  ghc-entropy-devel
BuildRequires:  ghc-failure-devel
BuildRequires:  ghc-fast-logger-devel
BuildRequires:  ghc-file-embed-devel
BuildRequires:  ghc-filesystem-conduit-devel
BuildRequires:  ghc-fsnotify-devel
BuildRequires:  ghc-ghc-paths-devel
BuildRequires:  ghc-hamlet-devel
BuildRequires:  ghc-hashable-devel
BuildRequires:  ghc-hjsmin-devel
BuildRequires:  ghc-hspec-devel
BuildRequires:  ghc-hspec-expectations-devel
BuildRequires:  ghc-html-conduit-devel
BuildRequires:  ghc-http-conduit-devel
BuildRequires:  ghc-http-date-devel
BuildRequires:  ghc-http-reverse-proxy-devel
BuildRequires:  ghc-http-types-devel
BuildRequires:  ghc-language-javascript-devel
BuildRequires:  ghc-lifted-base-devel
BuildRequires:  ghc-mime-mail-devel
BuildRequires:  ghc-mime-types-devel
BuildRequires:  ghc-monad-control-devel
BuildRequires:  ghc-monad-logger-devel
BuildRequires:  ghc-network-conduit-devel
BuildRequires:  ghc-optparse-applicative-devel
BuildRequires:  ghc-path-pieces-devel
BuildRequires:  ghc-pem-devel
BuildRequires:  ghc-persistent-devel
BuildRequires:  ghc-persistent-template-devel
BuildRequires:  ghc-pool-conduit-devel
BuildRequires:  ghc-primitive-devel
BuildRequires:  ghc-project-template-devel
BuildRequires:  ghc-publicsuffixlist-devel
BuildRequires:  ghc-pureMD5-devel
BuildRequires:  ghc-pwstore-fast-devel
BuildRequires:  ghc-resource-pool-devel
BuildRequires:  ghc-resourcet-devel
BuildRequires:  ghc-safe-devel
BuildRequires:  ghc-semigroups-devel
BuildRequires:  ghc-setenv-devel
BuildRequires:  ghc-shakespeare-devel
BuildRequires:  ghc-shakespeare-css-devel
BuildRequires:  ghc-shakespeare-i18n-devel
BuildRequires:  ghc-shakespeare-js-devel
BuildRequires:  ghc-shakespeare-text-devel
BuildRequires:  ghc-silently-devel
BuildRequires:  ghc-simple-sendfile-devel
BuildRequires:  ghc-skein-devel
BuildRequires:  ghc-socks-devel
BuildRequires:  ghc-split-devel
BuildRequires:  ghc-stringsearch-devel
BuildRequires:  ghc-system-fileio-devel
BuildRequires:  ghc-system-filepath-devel
BuildRequires:  ghc-tagged-devel
BuildRequires:  ghc-tagsoup-devel
BuildRequires:  ghc-tagstream-conduit-devel
BuildRequires:  ghc-tar-devel
BuildRequires:  ghc-tls-devel
BuildRequires:  ghc-tls-extra-devel
BuildRequires:  ghc-transformers-base-devel
BuildRequires:  ghc-unix-compat-devel
BuildRequires:  ghc-unordered-containers-devel
BuildRequires:  ghc-utf8-light-devel
BuildRequires:  ghc-utf8-string-devel
BuildRequires:  ghc-vault-devel
BuildRequires:  ghc-vector-devel
BuildRequires:  ghc-void-devel
BuildRequires:  ghc-wai-devel
BuildRequires:  ghc-wai-app-static-devel
BuildRequires:  ghc-wai-extra-devel
BuildRequires:  ghc-wai-logger-devel
BuildRequires:  ghc-wai-test-devel
BuildRequires:  ghc-warp-devel
BuildRequires:  ghc-word8-devel
BuildRequires:  ghc-xml-conduit-devel
BuildRequires:  ghc-xml-types-devel
BuildRequires:  ghc-xss-sanitize-devel
BuildRequires:  ghc-yaml-devel
BuildRequires:  ghc-yesod-devel
BuildRequires:  ghc-yesod-auth-devel
BuildRequires:  ghc-yesod-core-devel
BuildRequires:  ghc-yesod-default-devel
BuildRequires:  ghc-yesod-form-devel
BuildRequires:  ghc-yesod-json-devel
BuildRequires:  ghc-yesod-persistent-devel
BuildRequires:  ghc-yesod-routes-devel
BuildRequires:  ghc-yesod-static-devel
BuildRequires:  ghc-yesod-test-devel
BuildRequires:  ghc-zlib-bindings-devel
BuildRequires:  ghc-zlib-conduit-devel
ExclusiveArch:  %{ghc_arches_with_ghci}
# End cabal-rpm deps

%description
%{common_description}


%prep
%setup -q -n %{pkg_name}-%{version}


%build
%ghc_lib_build


%install
%ghc_lib_install


%ghc_devel_package

%ghc_devel_description


%ghc_devel_post_postun


%ghc_files LICENSE


%changelog
* Wed Mar 20 2013 Fedora Haskell SIG <haskell@lists.fedoraproject.org>
- spec file generated by cabal-rpm-0.7.0
